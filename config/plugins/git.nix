{
  plugins = {
    fugitive.enable = true;
    gitsigns = {
      enable = true;
      settings.trouble = true;
    };
    gitblame = {
      enable = true;
      settings = {
        date_format = "%r";
        # virtual_text_column = 80;
        delay = 25;
        # message_template = "<author> • <date> • <summary>";
        # highlight_group = "Question";
      };
    };
    lazygit.enable = true;
  };

  keymaps = [
    {
      mode = "n";
      key = "<leader>lg";
      action = "<CMD>LazyGit<CR>";
      options = {
        desc = "Open lazygit";
      };
    }
  ];
}
