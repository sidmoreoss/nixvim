{
  plugins.lualine = {
    enable = true;
    settings = {
      extensions = ["nvim-tree" "nvim-dap-ui" "toggleterm" "quickfix"];
      options = {
        globalstatus = true;
      };
    };
  };
}
