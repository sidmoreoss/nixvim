{
  plugins.oil = {
    enable = true;
    settings = {
      default_file_explorer = true;
      delete_to_trash = true;
      skip_confirm_for_simple_edits = true;
      watch_for_changes = true;
      float = {padding = 15;};
      view_options = {
        show_hidden = true;
        natural_order = true;
      };
      win_options = {
        wrap = true;
      };
      keymaps = {
        "<Esc>" = "actions.close";
      };
    };
  };
  keymaps = [
    {
      mode = "n";
      key = "-";
      action = "<CMD>Oil --float<CR>";
      options = {
        desc = "Open parent directory";
      };
    }
  ];
}
