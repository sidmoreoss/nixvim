{
  plugins.which-key = {
    enable = true; # If you want to enable then change timeoutLen to 10 in core/options.nix for better performance
    settings.show_keys = true;
  };
}
