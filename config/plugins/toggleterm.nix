{
  keymaps = [
    {
      mode = "n";
      key = "<leader>tt";
      action = "<CMD>lua require('toggleterm').toggle()<CR>";
      options = {
        desc = "Open";
      };
    }
    {
      mode = "t";
      key = "<leader>tt";
      action = "<C-\\><C-n><CMD>lua require('toggleterm').toggle()<CR>";
      options = {
        desc = "Close";
      };
    }
  ];
  plugins.toggleterm = {
    enable = true;
    settings.size = 40;
  };
}
