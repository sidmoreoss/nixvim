{pkgs, ...}: {
  extraPackages = with pkgs; [fd];

  plugins.telescope = {
    enable = true;
    keymaps = {
      "<leader>ff" = {
        action = "find_files";
        options.desc = "Find files telescope";
      };
      "<leader>fg" = {
        action = "live_grep";
        options.desc = "Live grep telescope";
      };
      "<leader>gf" = {
        action = "git_files";
        options.desc = "Git files telescope";
      };
      "<leader>vh" = {
        action = "help_tags";
        options.desc = "Help tags telescope";
      };
    };

    extensions = {
      ui-select.enable = true;
    };
  };
}
