{
  lib,
  pkgs,
  ...
}: {
  plugins.conform-nvim = {
    enable = true;
    settings = {
      formatters_by_ft = {
        asm = ["asmfmt"];
        c = ["astyle"];
        cpp = ["astyle"];
        css = ["prettierd" "prettier"];
        cmake = ["cmake_format"];
        go = ["goimports" "gofumpt" "golines"];
        html = ["prettierd" "prettier"];
        javascript = ["prettierd" "prettier"];
        javascriptreact = ["prettier"];
        json = ["prettier"];
        lua = ["stylua"];
        markdown = ["prettier"];
        nix = ["nixfmt"];
        python = ["isort" "black"];
        rust = ["rustfmt"];
        sh = ["shfmt"];
        typescript = ["prettierd" "prettier"];
        typescriptreact = ["prettier"];
        yaml = ["prettierd" "prettier"];
      };
      format_on_save = {
        timeout_ms = 5000;
        lsp_fallback = true;
      };
      formatters = {
        shfmt.command = lib.getExe pkgs.shfmt;
        nixfmt.command = lib.getExe pkgs.nixfmt-rfc-style;
        prettier.command = lib.getExe pkgs.nodePackages.prettier;
        prettierd.command = lib.getExe pkgs.prettierd;
        rustfmt.command = lib.getExe pkgs.rustfmt;
        stylua.command = lib.getExe pkgs.stylua;
        cmake_format.command = lib.getExe pkgs.cmake-format;
        isort.command = lib.getExe pkgs.isort;
        black.command = lib.getExe pkgs.black;
        astyle.command = lib.getExe pkgs.astyle;
        goimports.command = lib.getExe pkgs.gosimports;
        gofumpt.command = lib.getExe pkgs.gofumpt;
      };
    };
  };

  extraPackages = with pkgs; [
    # Formatters
    golines
  ];
}
