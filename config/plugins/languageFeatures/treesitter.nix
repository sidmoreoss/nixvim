{ ... }:
{
  plugins = {
    treesitter = {
      enable = true;
      folding = true;
      settings = {
        indent.enable = true;
        highlight.enable = true;

        incremental_selection = {
          enable = true;
          #
          #   keymaps = {
          #     init_selection = true;
          #     node_decremental = true;
          #     node_incremental = true;
          #     scope_incremental = true;
          #   };
        };
      };
    };

    treesitter-context.enable = true;

    treesitter-refactor = {
      enable = true;
      # highlightCurrentScope.enable = true;
      highlightDefinitions.enable = true;
      smartRename.enable = true;
    };
  };
}
