{ ... }:
{
  plugins.lsp = {
    enable = true;
    keymaps = {
      lspBuf = {
        K = "hover";
        "<leader>ca" = "code_action";
      };
    };
    servers = {
      # Bash
      bashls.enable = true;

      # CSS
      cssls.enable = true;

      # C/C++
      ccls.enable = true;
      clangd.enable = true;

      # Javascript/Typescript
      eslint.enable = true;

      # JSON
      jsonls.enable = true;

      # Latex
      ltex.enable = true;
      texlab.enable = true;

      # Lua
      lua_ls.enable = true;

      # Nix
      nixd.enable = true;

      # Python
      # pylsp.enable = true;
      pyright.enable = true;

      # Typescript
      ts_ls.enable = true;

      # Go
      gopls.enable = true;
    };
  };

  plugins.lsp-format.enable = true;
  plugins.lsp-lines.enable = true;
  plugins.lsp-status.enable = true;
}
