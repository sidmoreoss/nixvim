{
  plugins.trouble = {
    enable = true;
  };

  keymaps = [
    {
      mode = "n";
      key = "<leader>xq";
      action = "<cmd>TroubleToggle quickfix<CR>";
    }
  ];
}
