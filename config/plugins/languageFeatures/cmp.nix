{
  plugins = {
    luasnip.enable = true;
    friendly-snippets.enable = true;
    cmp-nvim-lsp.enable = true;
    cmp_luasnip.enable = true;
    cmp-path.enable = true;
    cmp-vsnip.enable = true;
    cmp-buffer.enable = true;

    cmp = {
      enable = true;

      settings = {
        experimental.ghost_test = true;

        snippet.expand = ''
          function(args)
            require('luasnip').lsp_expand(args.body)
          end
        '';

        performance = {
          debounce = 60;
          fetchingTimeout = 200;
          maxViewEntries = 30;
        };

        sources = [
          {name = "nvim_lsp";}
          {name = "luasnip";}
          {name = "path";}
          {name = "vsnip";}
          {name = "buffer";}
          {name = "cmp_ai";}
        ];

        mapping = {
          "<C-Space>" = "cmp.mapping.complete()";
          "<C-d>" = "cmp.mapping.scroll_docs(-4)";
          "<C-e>" = "cmp.mapping.close()";
          "<C-f>" = "cmp.mapping.scroll_docs(4)";
          "<CR>" = "cmp.mapping.confirm({ select = true })";
          "<S-Tab>" = "cmp.mapping(cmp.mapping.select_prev_item(), {'i', 's'})";
          "<Tab>" = "cmp.mapping(cmp.mapping.select_next_item(), {'i', 's'})";
        };
      };
    };

    cmp-ai = {
      enable = true;
      settings = {
        notify = false;
        max_lines = 1000;
        provider = "Codestral";
        provider_options = {
          model = "codestral-latest";
        };
      };
    };
  };
}
