{
  description = "A nixvim configuration";

  inputs = {
    nixvim.url = "github:nix-community/nixvim";
    flake-utils.url = "github:numtide/flake-utils";
    nypkgs.url = "github:yunfachi/nypkgs";
  };

  outputs = {
    nixpkgs,
    nixvim,
    flake-utils,
    nypkgs,
    ...
  }:
    flake-utils.lib.eachDefaultSystem (system: let
      nixvimLib = nixvim.lib.${system};
      pkgs = import nixpkgs {inherit system;};
      nixvim' = nixvim.legacyPackages.${system};

      ylib = nypkgs.lib.${system}; # Access the library functions from the nypkgs input for the specified system architecture.

      nvim = nixvim'.makeNixvimWithModule {
        inherit pkgs;
        module = {
          imports = ylib.umport {
            path = ./config;
            recursive = true;
          };
        };
      };
    in {
      checks = {
        # Run `nix flake check .` to verify that your config is not broken
        default = nixvimLib.check.mkTestDerivationFromNvim {
          inherit nvim;
          name = "A nixvim configuration";
        };
      };

      packages = {
        # Lets you run `nix run .` to start nixvim
        default = nvim;
      };
    });
}
